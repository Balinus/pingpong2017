"""
score calcule le score de chaque match et l'associe a un joueur et une équipe sur une base mensuelle
"""
function scorepingpong(df::DataFrame)

  dfmonth = groupby(df, :Date)

  ScoresCumulatif = DataFrame(Noms = @data(["Bleu", "Jaune", "Mauve", "Orange", "Rouge", "Vert"]),
        Noms2 = @data(["", "", "", "", "", ""]),
        Points = @data([0.0, 0.0, 0.0, 0.0, 0.0, 0.0]),
        )

  for idf = dfmonth # loop over each month
    # Create DataFrame for month (everyname from Teams dict)
    pts = zeros(Float64, length(unique(keys(Teams))), 1)
    matchs = zeros(Float64, length(unique(keys(Teams))), 1)
    moy = zeros(Float64, length(unique(keys(Teams))), 1)
    joueurs = unique(keys(Teams))
    equipe = [val for val in values(Teams)]
    Scores = DataFrame([joueurs equipe pts matchs moy])
    Scores = DataFrame([collect(keys(Teams)) [val for val in values(Teams)] pts matchs moy])
    rename!(Scores, [:x1, :x2, :x3, :x4, :x5], [:Noms, :Équipe, :Points, :Matchs, :Moy])

    # Some type problems apparently...
    Scores[:Noms] = convert.(String, Scores[:Noms])
    Scores[:Équipe] = convert.(String, Scores[:Équipe])
    Scores[:Points] = convert.(Float64, Scores[:Points])
    Scores[:Matchs] = convert.(Float64, Scores[:Matchs])
    Scores[:Moy] = convert.(Float64, Scores[:Moy])

    for ipartie = 1:size(idf, 1) # loop over each game

      S = scorepartie(idf[ipartie,:])

      # Accumuler pour le bon joueur
      idx1 = find(Scores[:Noms] .== String(S[:Noms][1]))
      idx2 = find(Scores[:Noms] .== String(S[:Noms][2]))

      Scores[idx1, :Points] += S[:Score][1]
      Scores[idx1, :Matchs] += 1
      Scores[idx2, :Points] += S[:Score][2]
      Scores[idx2, :Matchs] += 1
    end
    Scores[:Moy] = Scores[:Points] ./ Scores[:Matchs] # normalize

    # Les boulets (ceux qui ne jouent pas!)
    println("")
    println("----------------------------")
    println(string("Les boulets ", idf[1, 1]))
    println("----------------------------")
    # println(idf[:Date][1])
    println(Scores[:Noms][find(isnan(Scores[:Moy]))])
    Scores[:Moy][isnan(Scores[:Moy])] = NA # convert to NA

    # Sort dataframe by best players
    T = sort(Scores, cols = :Moy, rev = true)
    T = T[~isna(T[:,:Moy]),:]
    S = T[T[:Matchs] .>= 2.,:]

    println("")
    println("----------------------------")
    println(string("Meilleurs joueurs ", idf[1, 1]))
    println("----------------------------")
    println(S[1:7, :])

    # on arrondi pour exporter vers CSV
    T[:Points] = round(T[:Points], 2)
    T[:Moy] = round(T[:Moy], 2)
    # Sauvegarde
    fileJoueurs = string("/home/proy/Julia/Projets/PingPong/Resultats/Joueurs", Int(idf[1, 1]),".csv")
    CSV.write(fileJoueurs, T)

    # Sub-dataframe, séparation par équipe
    equipedf = groupby(Scores, :Équipe)

    ScoresEquipe = DataFrame(Noms = @data(["Bleu", "Jaune", "Mauve", "Orange", "Rouge", "Vert"]),
          Noms2 = @data(["", "", "", "", "", ""]),
          Points = @data([0.0, 0.0, 0.0, 0.0, 0.0, 0.0]),
          Points_norm = @data([0.0, 0.0, 0.0, 0.0, 0.0, 0.0]),
          Points_final = @data([0.0, 0.0, 0.0, 0.0, 0.0, 0.0]),
          Matchs = @data([0.0, 0.0, 0.0, 0.0, 0.0, 0.0]),
          Diff = @data([0.0, 0.0, 0.0, 0.0, 0.0, 0.0]),
          FUN = @data([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
          )

    for iequipe = equipedf # loop over teams
      idx = find(ScoresEquipe[:Noms] .== String(iequipe[1, :Équipe]))
      ScoresEquipe[idx, :Noms2] = TeamDef[iequipe[1, :Équipe]]
      ScoresEquipe[idx, :Points] = sum(dropna(iequipe[:Moy]))
      ScoresEquipe[idx, :Points_norm] = ScoresEquipe[idx, :Points] ./ 7
      ScoresEquipe[idx, :Matchs] = sum(iequipe[:Matchs])
    end
    # on classe pour associer le facteur FUN! avec le nombre de Matchs joués
    ScoresEquipe_sort = sort(ScoresEquipe, cols = :Matchs, rev = true)

    # Facteur FUN
    FUN = 1.10:-0.02:1.00
    ScoresEquipe_sort[:FUN] = FUN

    # on vérifie s'il y a égalité entre 2 équipes pour le nombre de match joués
    ScoresEquipe_sort[2:end, :Diff] = diff(ScoresEquipe_sort[:Matchs])

    for y = 2:size(ScoresEquipe_sort, 1)
      if iszero(ScoresEquipe_sort[y, :Diff])
        ScoresEquipe_sort[y, :FUN] = ScoresEquipe_sort[y - 1, :FUN]
      end
    end

    # Application du facteur FUN!
    ScoresEquipe_sort[:Points_final] = ScoresEquipe_sort[:Points_norm] .* ScoresEquipe_sort[:FUN]
    # on classe selon les points finaux
    ScoresEquipe_sort_final = sort(ScoresEquipe_sort, cols = :Points_final, rev = true)

    println("")
    println("----------------------------")
    println(string("Meilleures équipes ", idf[1, 1]))
    println("----------------------------")
    println(ScoresEquipe_sort_final)

    for y = 1:size(ScoresCumulatif, 1)
      idx = find(ScoresCumulatif[:Noms] .== String(ScoresEquipe_sort_final[y, :Noms]))
      ScoresCumulatif[idx, :Noms2] = ScoresEquipe_sort_final[y, :Noms2]
      ScoresCumulatif[idx, :Points] += ScoresEquipe_sort_final[y, :Points_final]
    end


    # On arrondie pour exporter vers CSV
    ScoresEquipe_sort_final[:Points] = round(ScoresEquipe_sort_final[:Points], 2)
    ScoresEquipe_sort_final[:Points_norm] = round(ScoresEquipe_sort_final[:Points_norm], 2)
    ScoresEquipe_sort_final[:Points_final] = round(ScoresEquipe_sort_final[:Points_final], 2)

    # Sauvegarde
    fileEquipes = string("/home/proy/Julia/Projets/PingPong/Resultats/Equipes", Int(idf[1, 1]),".csv")
    CSV.write(fileEquipes, ScoresEquipe_sort_final)

  end

  # Print score cumulatif et sauvegarde

  ScoresCumulatif = sort(ScoresCumulatif, cols = :Points, rev = true)

  println("")
  println("----------------------------")
  println("Scores cumulatif!")
  println("----------------------------")
  println(ScoresCumulatif)

  # Sauvegarde
  ScoresCumulatif[:Points] = round(ScoresCumulatif[:Points], 2)
  fileCumul = string("/home/proy/Julia/Projets/PingPong/Resultats/ScoreCumulatif.csv")
  CSV.write(fileCumul, ScoresCumulatif)

end

function scorepartie(partie)
  # Facteurs égo
  if partie[1, :Niveau_initial1] .> partie[1, :Niveau_initial2]
    fac1 = 0.9
    fac2 = 1.1
  elseif partie[1, :Niveau_initial1] .< partie[1, :Niveau_initial2]
    fac1 = 1.1
    fac2 = 0.9
  elseif partie[1, :Niveau_initial1] .== partie[1, :Niveau_initial2]
    fac1 = 1.0
    fac2 = 1.0
  end
  # Scores
  if partie[1, :Nom1] .== partie[1, :Gagnant]
    scorenom1 = 3 * fac1
    scorenom2 = 1 * fac2
  elseif partie[1, :Nom2] .== partie[1, :Gagnant]
    scorenom1 = 1 * fac1
    scorenom2 = 3 * fac2
  end

  S = DataFrame()
  S[:Noms] = [partie[1, :Nom1],  partie[1, :Nom2]]
  S[:Score] = [scorenom1, scorenom2]
  return S
end
