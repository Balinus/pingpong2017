using Revise
using DataFrames, ExcelReaders, CSV
 Revise.track("/home/proy/Julia/Projets/PingPong/functions_pingpong.jl")
  Revise.track("/home/proy/Julia/Projets/PingPong/Equipes.jl")
datafile = "/home/proy/Dropbox/Tournoi_de_ping_pong_Fun/2017/Statistique_Ping_Pong.xlsx"

include("Equipes.jl")
include("functions_pingpong.jl")

df = readxlsheet(DataFrame, datafile, "Stats_curated")

scorepingpong(df)
